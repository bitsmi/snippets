package com.bitsmi.snippets.logging.logback;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.ConsoleAppender;
import ch.qos.logback.core.rolling.DefaultTimeBasedFileNamingAndTriggeringPolicy;
import ch.qos.logback.core.rolling.FixedWindowRollingPolicy;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy;
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy;
import ch.qos.logback.core.util.FileSize;

public class LogbackDynamicLoggerTestCase 
{
	private static final String TEST_WORKSPACE_FOLDER = "test-workspace/logback";
	private static final String TEST_DATA_FOLDER = "test-data/logback";
	private static final String LOGGER_NAME = "dynamic_logger";

	@BeforeClass
	public static void setUp() throws IOException
	{
		// Clean test workspace
		FileUtils.deleteDirectory(Paths.get(TEST_WORKSPACE_FOLDER).toFile());
	}
	
	@Test
	public void loggingTest() throws IOException
	{
		LoggerContext context = (LoggerContext)LoggerFactory.getILoggerFactory();
		ch.qos.logback.classic.Logger logbackLog = context.getLogger(LOGGER_NAME);
		logbackLog.detachAndStopAllAppenders();
		logbackLog.setLevel(Level.WARN);
	    logbackLog.setAdditive(false);

	    PatternLayoutEncoder encoder = new PatternLayoutEncoder();
	    encoder.setContext(context);
	    encoder.setPattern("%msg%n");
	    encoder.start();
	    
		ConsoleAppender<ILoggingEvent> consoleAppender = buildConsoleAppender(context, encoder);
	    consoleAppender.start();

		RollingFileAppender<ILoggingEvent> fileAppender = buildTimeBasedFileAppender(context, encoder, "test");
		fileAppender.start();
		
	    logbackLog.addAppender(consoleAppender);
	    logbackLog.addAppender(fileAppender);
		
		Logger log = LoggerFactory.getLogger(LOGGER_NAME);
		log.info("Aquest missatge no hauria de sortir");
		log.warn("En canvi aquest si");
		
		List<String> fileContents = Files.readAllLines(Paths.get(TEST_WORKSPACE_FOLDER, "test.log"));
		Assert.assertEquals(1, fileContents.size());
		Assert.assertEquals("En canvi aquest si", fileContents.get(0));
	}
	
	@Test
	public void sizeRollingTest() throws IOException
	{
		LoggerContext context = (LoggerContext)LoggerFactory.getILoggerFactory();
		ch.qos.logback.classic.Logger logbackLog = context.getLogger(LOGGER_NAME);
		logbackLog.detachAndStopAllAppenders();
		logbackLog.setLevel(Level.WARN);
	    logbackLog.setAdditive(false);

	    PatternLayoutEncoder encoder = new PatternLayoutEncoder();
	    encoder.setContext(context);
	    encoder.setPattern("%msg%n");
	    encoder.start();
	    
		ConsoleAppender<ILoggingEvent> consoleAppender = buildConsoleAppender(context, encoder);
	    consoleAppender.start();

		RollingFileAppender<ILoggingEvent> fileAppender = buildSizeBasedFileAppender(context, encoder, "test-size");
		fileAppender.start();
		
	    logbackLog.addAppender(consoleAppender);
	    logbackLog.addAppender(fileAppender);
	    
		Logger log = LoggerFactory.getLogger(LOGGER_NAME);
		for(int i=0; i<=512; i++) {
			// Writes 2 bytes '0' and '\n'
	    	log.warn("0");
	    }
		
		Path rolledLog = Paths.get(TEST_WORKSPACE_FOLDER, "test-size.1.log");
		Assert.assertTrue(rolledLog.toFile().exists());
		Path currentLog = Paths.get(TEST_WORKSPACE_FOLDER, "test-size.log");
		Assert.assertTrue(currentLog.toFile().exists());
	}
	
	private ConsoleAppender<ILoggingEvent> buildConsoleAppender(LoggerContext context, PatternLayoutEncoder encoder)
	{
		ConsoleAppender<ILoggingEvent> consoleAppender = new ConsoleAppender<>();
		consoleAppender.setContext(context);
		consoleAppender.setEncoder(encoder);
	    consoleAppender.start();
	    
	    return consoleAppender;
	}
	
	private RollingFileAppender<ILoggingEvent> buildTimeBasedFileAppender(LoggerContext context, PatternLayoutEncoder encoder, String logName)
	{
		// Triggering policy
	    DefaultTimeBasedFileNamingAndTriggeringPolicy<ILoggingEvent> triggeringPolicy = new DefaultTimeBasedFileNamingAndTriggeringPolicy<>();
	    triggeringPolicy.setContext(context);
	    // Rolling policy
	    TimeBasedRollingPolicy<ILoggingEvent> rollingPolicy = new TimeBasedRollingPolicy<>();
	    rollingPolicy.setContext(context);
	    rollingPolicy.setFileNamePattern(TEST_WORKSPACE_FOLDER + "/" + logName + ".%d{yyyy-MM-dd}.log");
		rollingPolicy.setMaxHistory(2);
		rollingPolicy.setTimeBasedFileNamingAndTriggeringPolicy(triggeringPolicy);
		triggeringPolicy.setTimeBasedRollingPolicy(rollingPolicy);
		// Appender
		RollingFileAppender<ILoggingEvent> fileAppender = new RollingFileAppender<>();
		fileAppender.setContext(context);
		fileAppender.setEncoder(encoder);
		fileAppender.setAppend(true);
		fileAppender.setFile(TEST_WORKSPACE_FOLDER + "/" + logName + ".log");
		fileAppender.setRollingPolicy(rollingPolicy);
		rollingPolicy.setParent(fileAppender);
		
		rollingPolicy.start();
		
		return fileAppender;
	}
	
	private RollingFileAppender<ILoggingEvent> buildSizeBasedFileAppender(LoggerContext context, PatternLayoutEncoder encoder, String logName)
	{
		// Triggering policy
		SizeBasedTriggeringPolicy<ILoggingEvent> triggeringPolicy = new SizeBasedTriggeringPolicy<>();
		triggeringPolicy.setContext(context);
		triggeringPolicy.setMaxFileSize(FileSize.valueOf("1KB"));
	    // Rolling policy
	    FixedWindowRollingPolicy rollingPolicy = new FixedWindowRollingPolicy();
	    rollingPolicy.setContext(context);
		rollingPolicy.setFileNamePattern(TEST_WORKSPACE_FOLDER + "/" + logName + ".%i.log");
		rollingPolicy.setMinIndex(1);
		rollingPolicy.setMaxIndex(2);
		// Appender
		RollingFileAppender<ILoggingEvent> fileAppender = new RollingFileAppender<>();
		fileAppender.setContext(context);
		fileAppender.setEncoder(encoder);
		fileAppender.setAppend(true);
		fileAppender.setFile(TEST_WORKSPACE_FOLDER + "/" + logName + ".log");
		fileAppender.setRollingPolicy(rollingPolicy);
		fileAppender.setTriggeringPolicy(triggeringPolicy);
		rollingPolicy.setParent(fileAppender);
		
		triggeringPolicy.start();
		rollingPolicy.start();
		
		return fileAppender;
	} 
}
