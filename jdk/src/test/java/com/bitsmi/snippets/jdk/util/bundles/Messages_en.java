package com.bitsmi.snippets.jdk.util.bundles;

public class Messages_en extends Messages 
{
	@Override
	protected void populateData()
	{
		data.put("messages.car","car");
		data.put("messages.house","house");
	}
}
