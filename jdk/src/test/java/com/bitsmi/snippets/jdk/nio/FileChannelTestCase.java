package com.bitsmi.snippets.jdk.nio;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class FileChannelTestCase 
{
	private static final String TEST_WORKSPACE_FOLDER = "test-workspace/nio";
	private static final String TEST_DATA_FOLDER = "test-data/nio";
	
	@BeforeClass
	public static void initialize() throws IOException
	{
		File dataDir = new File(TEST_WORKSPACE_FOLDER);
		
		if(!dataDir.exists()){
			FileUtils.forceMkdir(dataDir);
		}
		// Borrado del directorio de datos
		else{
			FileUtils.cleanDirectory(dataDir);
		}
	}
	
	@Test
	public void writeFileChannelTest() throws FileNotFoundException, IOException
	{
		Path filePath = Paths.get(TEST_WORKSPACE_FOLDER, "writeFileChannelTest.txt");
		try(RandomAccessFile dataFile = new RandomAccessFile(filePath.toFile(), "rw")){
			FileChannel fileChannel = dataFile.getChannel();
			
			ByteBuffer writeBuf = ByteBuffer.allocate(20);
			// Preparación del buffer para escribir los datos
			writeBuf.clear();
			writeBuf.put("1234567890".getBytes());
			writeBuf.put("abcdefghij".getBytes());
			
			// Es necesario invertir el buffer para la operación de escritura en el fichero
			writeBuf.flip();
	
			while(writeBuf.hasRemaining()){
				fileChannel.write(writeBuf);
			}
		}
		
		List<String> fileContents = Files.readAllLines(filePath);
		Assert.assertEquals("1234567890abcdefghij", fileContents.get(0));
	}
	
	@Test
	public void randomWriteFileChannelTest() throws FileNotFoundException, IOException
	{
		Path filePath = Paths.get(TEST_WORKSPACE_FOLDER, "randomWriteFileChannelTest.txt");
		
		// Se introducen algunos datos en el fichero para la prueba
		byte[] testData = "abcdefghijklmnopqrstuvwx".getBytes(StandardCharsets.UTF_8);
		FileUtils.writeByteArrayToFile(filePath.toFile(), testData);
		
		try(RandomAccessFile dataFile = new RandomAccessFile(filePath.toFile(), "rw")){
			FileChannel fileChannel = dataFile.getChannel();
			
			ByteBuffer buf = ByteBuffer.allocate(20);
			// Preparación del buffer para escribir los datos
			buf.clear();
			buf.put("0123456789".getBytes());
			
			// Es necesario invertir el buffer para la operación de escritura en el fichero
			buf.flip();
	
			// Situar el puntero de escritura en la posición 10
			fileChannel.position(10);
			while(buf.hasRemaining()){
				fileChannel.write(buf);
			}
		}
		
		List<String> fileContents = Files.readAllLines(filePath);
		Assert.assertEquals("abcdefghij0123456789uvwx", fileContents.get(0));
	}
	
	@Test
	public void readFileChannelTest() throws FileNotFoundException, IOException
	{
		Path filePath = Paths.get(TEST_DATA_FOLDER, "readFileChannelTest.txt");
		try(RandomAccessFile dataFile = new RandomAccessFile(filePath.toFile(), "r")){
			FileChannel fileChannel = dataFile.getChannel();
			
			int valueLength = 20;
			
			ByteBuffer readBuf = ByteBuffer.allocate(valueLength);
			int bytesReaded=0;
			int count = 0;
			do{
				count = fileChannel.read(readBuf);
				bytesReaded += count;
			}while(count!=-1 && bytesReaded<valueLength);
			
			String readedValue = new String(readBuf.array());
			Assert.assertEquals("1234567890abcdefghij", readedValue);
		}
	}
	
	@Test
	public void randomReadFileChannelTest() throws FileNotFoundException, IOException
	{
		Path filePath = Paths.get(TEST_DATA_FOLDER, "randomReadFileChannelTest.txt");
		try(RandomAccessFile dataFile = new RandomAccessFile(filePath.toFile(), "r")){
			FileChannel fileChannel = dataFile.getChannel();
			
			int valueLength = 5;
			
			ByteBuffer readBuf = ByteBuffer.allocate(5);
			
			// Situar el puntero de lectura en la posición 10
			fileChannel.position(10);
			
			StringBuilder result = new StringBuilder();
			int bytesReaded=0;
			int count = 0;
			do{
				count = fileChannel.read(readBuf);
				result.append(new String(readBuf.array()));
				bytesReaded += count;
			}while(count!=-1 && bytesReaded<valueLength);
			
			Assert.assertEquals("abcde", result.toString());
		}
	}
}
