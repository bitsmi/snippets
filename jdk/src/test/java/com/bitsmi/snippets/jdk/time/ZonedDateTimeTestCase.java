package com.bitsmi.snippets.jdk.time;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class ZonedDateTimeTestCase 
{
	private TestData data;

	public ZonedDateTimeTestCase(TestData data) 
	{
		this.data = data;
	}
	
	@Test
	public void nonDstConversionTest()
	{
		int[] calendarValues = data.getTime();
		Calendar srcCalendar = new GregorianCalendar(calendarValues[0], calendarValues[1], calendarValues[2], calendarValues[3], calendarValues[4], calendarValues[5]);
		DateFormat fmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String srcDate = fmt.format(srcCalendar.getTime());
		
//		Assert.assertEquals("01/01/2018 12:00:00", srcDate);
		
		ZoneId srcTz = ZoneId.of(data.getSrcTimezone());
		DateTimeFormatter nyTzFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss").withZone(srcTz);
		ZonedDateTime srcDateTime = ZonedDateTime.parse(srcDate, nyTzFormatter);
		
		ZoneId targetTz = ZoneId.of(data.getTargetTimezone());
		ZonedDateTime targetDateTime = srcDateTime.withZoneSameInstant(targetTz);
		
		Calendar targetCalendar = Calendar.getInstance();
		targetCalendar.setTime(Date.from(targetDateTime.toLocalDateTime().toInstant(targetDateTime.getOffset())));
		
		Assert.assertEquals(18, targetCalendar.get(Calendar.HOUR_OF_DAY));
		
		System.out.println("SRC: " + srcCalendar.getTimeInMillis());
		System.out.println("TARGET: " + targetCalendar.getTimeInMillis());
	}
	
	@Parameters(name="data_{index}")
	public static Collection<?> provideTestData()
	{
		List<TestData> data = new LinkedList<>();
		data.add(new TestData()
				.setSrcTimezone("America/New_York")
				.setTargetTimezone("Europe/Amsterdam")
				.setTime(new int[] {2018, 3, 24, 12, 0, 0}));
		
		data.add(new TestData()
				.setSrcTimezone("America/New_York")
				.setTargetTimezone("Europe/Amsterdam")
				.setTime(new int[] {2018, 0, 1, 12, 0, 0}));
		
		return data;
	}
	
	private static class TestData
	{
		private String srcTimezone;
		private String targetTimezone;
		private int[] time;
		
		public String getSrcTimezone() 
		{
			return srcTimezone;
		}
		
		public TestData setSrcTimezone(String srcTimezone) 
		{
			this.srcTimezone = srcTimezone;
			return TestData.this;
		}
		
		public String getTargetTimezone() 
		{
			return targetTimezone;
		}
		
		public TestData setTargetTimezone(String targetTimezone) 
		{
			this.targetTimezone = targetTimezone;
			return TestData.this;
		}
		
		public int[] getTime() 
		{
			return time;
		}
		
		public TestData setTime(int[] time) 
		{
			this.time = time;
			return TestData.this;
		}
	}
}
