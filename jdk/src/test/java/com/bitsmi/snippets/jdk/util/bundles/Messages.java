package com.bitsmi.snippets.jdk.util.bundles;

import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.ResourceBundle;

public class Messages extends ResourceBundle 
{
	HashMap<String, String> data; 
			
			
	public Messages()
	{
		data = new HashMap<String, String>(); 
		populateData();
	}
	
	protected void populateData()
	{
		data.put("messages.car","coche");
		data.put("messages.house","casa");
	}
	
	@Override
	protected Object handleGetObject(String key) 
	{
		return data.get(key);
	}

	@Override
	public Enumeration<String> getKeys() 
	{
		return Collections.enumeration(data.keySet());
	}
}
