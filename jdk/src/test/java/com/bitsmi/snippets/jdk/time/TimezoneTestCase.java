package com.bitsmi.snippets.jdk.time;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TimezoneTestCase 
{
	// Formato por defecto GMT+1
	private DateFormat normalFormat;
	// Formato UTC GMT+0 y ignorando DST
	private DateFormat utcFormat;
	
	/**
	 * Inicialización de los formatos de fecha GMT+1 y UTC
	 */
	@Before
	public void initializeTest()
	{
		normalFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		TimeZone utc = TimeZone.getTimeZone("UTC");
		utcFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	    utcFormat.setTimeZone(utc);
	}
	
	@Test
	public void gmt2utcTimezoneTest() throws Exception
	{
		// Antes del cambio de hora
		Date preDate = normalFormat.parse("26/10/2014 01:00:00");
		// Después del cambio de hora
		Date postDate = normalFormat.parse("26/10/2014 04:00:00");

		// Diferencia -2h (franja horaria + DST)
	    Assert.assertEquals("25/10/2014 23:00:00", utcFormat.format(preDate));
	    // Diferencia -1h (sólo franja horaria)
	    Assert.assertEquals("26/10/2014 03:00:00", utcFormat.format(postDate));
	}
	
	@Test
	public void utc2gmtTimezoneTest() throws Exception
	{
		// Antes del cambio de hora
	    Date preDate = utcFormat.parse("25/10/2014 23:00:00");
	    // Después del cambio de hora
	    Date postDate = utcFormat.parse("26/10/2014 03:00:00");
	    // Diferencia -2h (franja horaria + DST)
	    Assert.assertEquals("26/10/2014 01:00:00", normalFormat.format(preDate));
	    // Diferencia -1h (sólo franja horaria)
	    Assert.assertEquals("26/10/2014 04:00:00", normalFormat.format(postDate));
	}
}
