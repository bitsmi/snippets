package com.bitsmi.snippets.jdk.reflection;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class ParameterizedTypesTestCase 
{
	@Test
	public void parameterizedMethodParametersTest() throws Exception
	{
		Method method = SimpleClass.class.getMethod("parameterizedMethod", List.class);
		Parameter param = method.getParameters()[0];
		Type type = param.getParameterizedType();
		if(!(type instanceof ParameterizedType)) {
			Assert.fail();
		}
		ParameterizedType parameterizedType = (ParameterizedType)type;
		Type[] generics = parameterizedType.getActualTypeArguments();
		
		Assert.assertEquals(1, generics.length);
		Assert.assertEquals(String.class, (Class<?>)generics[0]);
	}
	
	@Test
	public void genericMethodParametersTest() throws Exception
	{
		Method method = GenericClass.class.getMethod("parameterizedMethod", List.class);
		Parameter param = method.getParameters()[0];
		Type type = param.getParameterizedType();
		if(!(type instanceof ParameterizedType)) {
			Assert.fail();
		}
		ParameterizedType parameterizedType = (ParameterizedType)type;
		Type[] generics = parameterizedType.getActualTypeArguments();
		
		Assert.assertEquals(1, generics.length);
		Assert.assertTrue(TypeVariable.class.isAssignableFrom(generics[0].getClass()));
	}
	
	@Test
	public void concreteClassTypeTest() throws Exception
	{
		Type type = ConcreteClass.class.getGenericSuperclass();
		if(!(type instanceof ParameterizedType)) {
			Assert.fail();
		}
		
		Type[] generics = ((ParameterizedType)type)
				.getActualTypeArguments();
		
		Assert.assertEquals(1, generics.length);
		Assert.assertEquals(String.class, (Class<?>)generics[0]);		
	}
	
	@Test
	public void genericClassTypeTest() throws Exception
	{
		Type[] generics = GenericClass.class.getTypeParameters();
		
		Assert.assertEquals(1, generics.length);
		Assert.assertTrue(generics[0] instanceof TypeVariable);		
	}
	
	@Test
	public void simpleClassTypeTest() throws Exception
	{
		Type type = SimpleClass.class.getGenericSuperclass();
		if(!(type instanceof Class)) {
			Assert.fail();
		}		
	}
	
	private static class SimpleClass
	{
		public void parameterizedMethod(List<String> params)
		{
			
		}
	}
	
	public static class GenericClass<T>
	{
		public void parameterizedMethod(List<T> params)
		{
			
		}
	}
	
	public static class ConcreteClass extends GenericClass<String>
	{
		public void parameterizedMethod(List<String> params)
		{
			
		}
	}
}
