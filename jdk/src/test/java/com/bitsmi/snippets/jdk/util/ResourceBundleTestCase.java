package com.bitsmi.snippets.jdk.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.junit.Assert;
import org.junit.Test;

public class ResourceBundleTestCase 
{
	@Test
	public void defaultResourceBundleTest()
	{
		ResourceBundle bundle = ResourceBundle.getBundle("com.bitsmi.snippets.jdk.util.bundles.Messages");
		String message = bundle.getString("messages.car");
		Assert.assertEquals("coche", message);
		
		message = bundle.getString("messages.house");
		Assert.assertEquals("casa", message);
	}
	
	@Test
	public void localeResourceBundleTest()
	{
		ResourceBundle bundle = ResourceBundle.getBundle("com.bitsmi.snippets.jdk.util.bundles.Messages", new Locale("en"));
		String message = bundle.getString("messages.car");
		Assert.assertEquals("car", message);
		
		message = bundle.getString("messages.house");
		Assert.assertEquals("house", message);
	}
	
	@Test
	public void customControlBundleTest()
	{
		/* Recuperación de los recursos. A la creación del objetoo ResourceBundle se le proporciona un componente ResourceBundle.Control
		 * que permite personalizar la localización y recuperación de los recursos
		 */
		ResourceBundle bundle = ResourceBundle.getBundle("bundles.Messages", new Locale("en"), new ResourceBundle.Control()
		{
			/**
			 * Sobreescritura del método de la API que permite localizar un fichero de recuros a través
			 * de su nombre base y el Locale. Permitirá modificar la estructura del nombre del fichero
			 */
			@Override
			public String toBundleName(String baseName, Locale locale) 
			{
				if (locale == Locale.ROOT) {
	                return baseName;
	            }

	            String language = locale.getLanguage();
	            String country = locale.getCountry();
	            String variant = locale.getVariant();

	            if (language == "" && country == "" && variant == "") {
	                return baseName;
	            }
	            StringBuilder sb = new StringBuilder(baseName);
	            // Adaptación del standard. Se utilizará "-" en vez de "_" como separador despues del baseName
	            sb.append('-');
	            // El resto del formato del nombre sigue el estándar
	            if (variant != "") {
	                sb.append(language).append('_').append(country).append('_').append(variant);
	            } else if (country != "") {
	                sb.append(language).append('_').append(country);
	            } else {
	                sb.append(language);
	            }

	            return sb.toString();
			}

			/**
			 * Sobreescritura del método de la API que recuperar un fichero de recuros desde una ubicació predefinida. 
			 * Permitirá modificar la ubicació estándar del fichero
			 */
			@Override
			public ResourceBundle newBundle(String baseName, Locale locale, String format, ClassLoader loader, boolean reload) throws IllegalAccessException, InstantiationException, IOException 
			{
				// Sólo se recuperarán bundles en formato properties
				if(!"java.properties".equals(format)){
					throw new IllegalArgumentException("unknown format: " + format); 
				}
				
				String bundleName = toBundleName(baseName, locale);
				String resourceName = toResourceName(bundleName, "properties");
				// Los ficheros se localizarán dentro del directorio "data" que es relativo al directorio de la aplicación
				File bundleFile = new File("test-data/util/" + resourceName);
				
				if(!bundleFile.exists()){
					return null;
				}

				// En caso de que el fichero exista, se retornará el conjunto de recursos
				try(FileInputStream stream=new FileInputStream(bundleFile)){
					return new PropertyResourceBundle(stream);
				}
			}
		});
		
		// A partir de aqui se podrán usar los recursos recuperados
		String message = bundle.getString("messages.car");
		Assert.assertEquals("car", message);
		
		message = bundle.getString("messages.house");
		Assert.assertEquals("house", message);
	}
}
