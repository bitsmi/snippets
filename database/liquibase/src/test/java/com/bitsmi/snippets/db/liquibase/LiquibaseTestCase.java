package com.bitsmi.snippets.db.liquibase;

import java.io.File;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;

public class LiquibaseTestCase 
{
	private static final String TEST_WORKSPACE_FOLDER = "test-workspace/liquibase";
	private static final String TEST_DATA_FOLDER = "test-data/liquibase";
	
	private static final String DB_PROVIDER = "org.apache.derby.jdbc.EmbeddedDriver";
	private static final String DB_DIR = TEST_WORKSPACE_FOLDER + "/database/store";
	private static final String DB_URL = "jdbc:derby:" + DB_DIR + "/database;create=true";
	private static final String DB_USER = "admin";
	private static final String DB_PASSWORD = "admin";
	
	private static Connection connection;
	
	private final Logger log = LoggerFactory.getLogger(LiquibaseTestCase.class);
	
	@BeforeClass
	public static void setUp() throws Exception
	{
		// Clean test workspace
		File workspaceFolder = Paths.get(TEST_WORKSPACE_FOLDER).toFile();
		FileUtils.deleteDirectory(workspaceFolder);
		FileUtils.forceMkdir(workspaceFolder);
		
		Class.forName(DB_PROVIDER);						
		connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
		connection.setAutoCommit(false);
	}
	
	@AfterClass
	public static void testCaseFinalization() throws Exception
	{
		if(connection!=null){
			connection.close();
		}
	}
	
	@Test
	public void databaseCreationTest() throws Exception
	{
		Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(connection));
		Liquibase liquibase = null;
		try{
			liquibase = new Liquibase("com/bitsmi/snippets/db/liquibase/database_changelog.xml", new ClassLoaderResourceAccessor(), database);
			// Es pot especificat una etiqueta a mode de guard per llançar uns determinats changesets (mirar tag changeset en la definició  xml)
			liquibase.update((String)null);
		}
		finally{
			if(liquibase!=null){
				/* S'esborra el flag que indica que s'est� actualitzant la BDD. Això permet que un posterior procés
				 * d'actualització pugui dur-se a terme
				 */
				liquibase.forceReleaseLocks();
			}
		}
		
		assertQueryResult("select NAME from TEST_TABLE1 where ID=1", "VALUE_1");
	}
	
	private void assertQueryResult(String query, Object expected) throws SQLException
	{
		try(PreparedStatement ps = connection.prepareStatement(query)){
			boolean hasResults = ps.execute();
			if(!hasResults) {
				Assert.fail();
			}
			
			ResultSet rs = ps.getResultSet();
			rs.next();
			Object actual = rs.getObject(1);
			
			// Commmit is needed because AutoCommit = false
			connection.commit();
			
			Assert.assertEquals(expected, actual);
		}
	}
}
