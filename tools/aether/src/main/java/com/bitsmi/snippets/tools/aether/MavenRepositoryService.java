package com.bitsmi.snippets.tools.aether;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.maven.repository.internal.MavenRepositorySystemUtils;
import org.eclipse.aether.DefaultRepositorySystemSession;
import org.eclipse.aether.RepositorySystem;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.collection.CollectRequest;
import org.eclipse.aether.connector.basic.BasicRepositoryConnectorFactory;
import org.eclipse.aether.graph.Dependency;
import org.eclipse.aether.graph.DependencyFilter;
import org.eclipse.aether.impl.DefaultServiceLocator;
import org.eclipse.aether.repository.LocalRepository;
import org.eclipse.aether.repository.RemoteRepository;
import org.eclipse.aether.resolution.ArtifactResult;
import org.eclipse.aether.resolution.DependencyRequest;
import org.eclipse.aether.resolution.VersionRangeRequest;
import org.eclipse.aether.resolution.VersionRangeResult;
import org.eclipse.aether.spi.connector.RepositoryConnectorFactory;
import org.eclipse.aether.spi.connector.transport.TransporterFactory;
import org.eclipse.aether.transport.file.FileTransporterFactory;
import org.eclipse.aether.transport.http.HttpTransporterFactory;
import org.eclipse.aether.util.StringUtils;
import org.eclipse.aether.util.artifact.JavaScopes;
import org.eclipse.aether.util.filter.DependencyFilterUtils;
import org.eclipse.aether.version.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MavenRepositoryService 
{
	private final Logger log = LoggerFactory.getLogger(MavenRepositoryService.class);
	
	private RepositorySystem system;
	private LocalRepository localRepository;
	private RemoteRepository remoteRepository;
	private DefaultRepositorySystemSession session;
	
	public void setUp(String localRepositoryLocation)
	{
		/* Los componentes de Aether implementan org.eclipse.aether.spi.locator.Service para facilitar el wiring manual
		 * y la utilización del DefaultServiceLocator preinicializado. Sólo se han de registrar el conector para los repositorios 
		 * y la factoria de transporters
		 */
        DefaultServiceLocator locator = MavenRepositorySystemUtils.newServiceLocator();
        locator.addService(RepositoryConnectorFactory.class, BasicRepositoryConnectorFactory.class);
        locator.addService(TransporterFactory.class, FileTransporterFactory.class);
        locator.addService(TransporterFactory.class, HttpTransporterFactory.class);

        locator.setErrorHandler(new DefaultServiceLocator.ErrorHandler()
        {
            @Override
            public void serviceCreationFailed(Class<?> type, Class<?> impl, Throwable exception)
            {
            	log.error("ERROR: {}", exception.getMessage(), exception);
            }
        } );

        system = locator.getService(RepositorySystem.class);
        session = MavenRepositorySystemUtils.newSession();
		
        localRepository = new LocalRepository(localRepositoryLocation);
		session.setLocalRepositoryManager(system.newLocalRepositoryManager(session, localRepository));
		
//	    session.setTransferListener(new ConsoleTransferListener());
//	    session.setRepositoryListener(new ConsoleRepositoryListener());
		
		// Especificación del repositorio remoto que se utilizará
		remoteRepository = new RemoteRepository.Builder("central", "default", "http://central.maven.org/maven2/").build();
	}
	
	/**
	 * Método de descarga de un artefacto y sus dependéncias a través de su descriptor
	 * @param artifactDescriptor String - Descriptor del artefacto a localizar
	 * @throws Exception
	 */
	public void downloadArtifact(String artifactDescriptor) throws Exception
    {
		Artifact artifact = new DefaultArtifact(artifactDescriptor);

        DependencyFilter classpathFlter = DependencyFilterUtils.classpathFilter(JavaScopes.COMPILE);

        CollectRequest collectRequest = new CollectRequest();
        collectRequest.setRoot(new Dependency(artifact, JavaScopes.COMPILE));
        // Es necesario indicar el repositorio remoto para que descargue el artefacto en caso de que no se encuentre en el local
        collectRequest.setRepositories(Arrays.asList(remoteRepository));

        DependencyRequest dependencyRequest = new DependencyRequest(collectRequest, classpathFlter);

        List<ArtifactResult> artifactResults = system.resolveDependencies(session, dependencyRequest).getArtifactResults();

        // Se listan los resultados de la descarga
        for(ArtifactResult artifactResult:artifactResults){
            log.info(artifactResult.getArtifact() + " resuelto en " + artifactResult.getArtifact().getFile());
        }
    }
	
	/**
	 * Método de resolución del path de un artefacto dentro del repositorio local de maven
	 * @param artifactDescriptor String - Descriptor del artefacto a localizar
	 * @return String - Ruta completa del artefacto indicado
	 * @throws Exception
	 */
	public String resolveLocalArtifactPath(String artifactDescriptor) throws Exception
	{
		Artifact artifact = new DefaultArtifact(artifactDescriptor);

		String repositoryLocation = localRepository.getBasedir().getAbsolutePath();
		String artifactRelativePath = session.getLocalRepositoryManager().getPathForLocalArtifact(artifact);
		File file = new File(repositoryLocation + "/" + artifactRelativePath);
		if(file.exists()){
			return file.getAbsolutePath();
		}
		else{
			throw new Exception("No se ha encontrado el artefacto " + artifactDescriptor + " en el respositorio local: " + localRepository.getBasedir().getAbsolutePath());
		}
	}
	
	/**
	 * Método de resolución de las dependéncias de un artefacto descargadas en el respositorio local
	 * @param artifactDescriptor
	 * @return
	 * @throws Exception
	 */
	public List<Artifact> resolveLocalArtifactDependencies(String artifactDescriptor) throws Exception
	{
		// Se construye el artefacto a partir de su descriptor
		Artifact artifact = new DefaultArtifact(artifactDescriptor);
		// Sólo se resolverán las dependencias de compilación
        DependencyFilter classpathFlter = DependencyFilterUtils.classpathFilter(JavaScopes.COMPILE);
        // Se crea la petición con el artefacto principal que se quiere c
        CollectRequest collectRequest = new CollectRequest();
        // Es necesario indicar el repositorio remoto para que descargue el artefacto en caso de que no se encuentre en el local
        collectRequest.setRoot(new Dependency(artifact, JavaScopes.COMPILE));

        try{
        	DependencyRequest dependencyRequest = new DependencyRequest(collectRequest, classpathFlter);
        	List<Artifact> artifacts = new ArrayList<>();
        	for(ArtifactResult elem:system.resolveDependencies(session, dependencyRequest).getArtifactResults()){
        		artifacts.add(elem.getArtifact());
        	}
        	
        	return artifacts;
        }
        catch(Exception e){
        	throw new Exception("No s'han pogut resoldre les dependencies de l'artefacte al respositori: " + artifactDescriptor);
        }
	}
	
	/**
	 * Método de resolución de los paths de las dependéncias de un artefacto descargadas en el respositorio local
	 * @param artifactDescriptor
	 * @return
	 * @throws Exception
	 */
	public List<String> resolveLocalArtifactDependenciesPaths(String artifactDescriptor) throws Exception
	{
		List<String> depsPaths = new ArrayList<>();
		for(Artifact dep:resolveLocalArtifactDependencies(artifactDescriptor)){
			depsPaths.add(dep.getFile().getAbsolutePath());
		}
		
		return depsPaths;
	}
	
	/**
	 * Método de resolución de la versión más alta disponible para un artefacto dentro del respositorio local
	 * @param groupId
	 * @param artifactId
	 * @return
	 * @throws Exception
	 */
	public String resolveLocalHighestVersion(String groupId, String artifactId) throws Exception
	{
		// Todas las versiones
		return resolveLocalHighestVersion(groupId, artifactId, "[0,)");
	}
	
	/**
	 * Método de resolución de la versión más alta, dentro de un rango determinado, disponible para un artefacto dentro del respositorio local
	 * @param groupId
	 * @param artifactId
	 * @param range String - Rango sobre el que se realizará la consulta expresado como intervalo
	 * @return
	 * @throws Exception
	 */
	public String resolveLocalHighestVersion(String groupId, String artifactId, String range) throws Exception
	{
		String minVersion = null;
		String maxVersion = null;
		if(range!=null){
			Pattern rangePattern = Pattern.compile("^[\\[\\(](\\S*),(\\S*)[\\]\\)]$");
			Matcher matcher = rangePattern.matcher(range);
			if(matcher.matches()){
				minVersion = !StringUtils.isEmpty(matcher.group(1)) ? matcher.group(1) : null;
				maxVersion = !StringUtils.isEmpty(matcher.group(2)) ? matcher.group(2) : null;
				if(minVersion!=null && maxVersion!=null && minVersion.compareTo(maxVersion)>0){
					throw new Exception("El formato del rango de versiones no es correcto");
				}
			}
			else{
				throw new Exception("El formato del rango de versiones no es correcto");
			}
		}
		
		String repositoryLocation = localRepository.getBasedir().getAbsolutePath();
		String artifactFolderPath = getBasePathForArtifact(groupId, artifactId);
		File[] artifactsFolders = new File(repositoryLocation + "/" + artifactFolderPath)
				.listFiles((FileFilter)DirectoryFileFilter.DIRECTORY);
		String foundVersion = null;
		for(File file:artifactsFolders){
			String artifactVersion = file.getName();
			if(foundVersion==null || artifactVersion.compareTo(foundVersion)>0){
				if((minVersion==null || minVersion.compareTo(artifactVersion)<0)
						&& (maxVersion==null || maxVersion.compareTo(artifactVersion)>0)){
					foundVersion = artifactVersion;
				}
			}
		}
		
		return foundVersion;
	}
	
	/**
	 * Método de resolución de la versión más alta disponible para un artefacto, consultando al repositorio remoto si hace falta
	 * @param groupId
	 * @param artifactId
	 * @return
	 * @throws Exception
	 */
	public String resolveHighestVersion(String groupId, String artifactId) throws Exception
	{
		// Todas las versiones
		return resolveHighestVersion(groupId, artifactId, "[0,)");
	}
	
	/**
	 * Método de resolución de la versión más alta, dentro de un rango determinado, disponible para un artefacto, 
	 * consultando al repositorio remoto si hace falta
	 * @param groupId
	 * @param artifactId
	 * @param range String - Rango sobre el que se realizará la consulta expresado como intervalo
	 * @return
	 * @throws Exception
	 */
	public String resolveHighestVersion(String groupId, String artifactId, String range) throws Exception
	{
		String artifactDescriptor = groupId + ":" + artifactId + ":" + range;
		Artifact artifact = new DefaultArtifact(artifactDescriptor);
		
		VersionRangeRequest request = new VersionRangeRequest();
        request.setArtifact(artifact);
    	request.setRepositories(Arrays.asList(remoteRepository));

        VersionRangeResult result = system.resolveVersionRange(session, request);

        Version version = result.getHighestVersion();
        log.info("Versión encontrada " + version + " en el repositorio " + result.getRepository(version));
        
        return version.toString();
	}
	
	private String getBasePathForArtifact(String groupId, String artifactId)
    {
        StringBuilder path = new StringBuilder();
        path.append(groupId.replace('.', '/')).append('/');
        path.append(artifactId);        

        return path.toString();
    }
}
