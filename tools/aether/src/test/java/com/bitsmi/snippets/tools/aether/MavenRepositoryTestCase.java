package com.bitsmi.snippets.tools.aether;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class MavenRepositoryTestCase 
{
	private static final String TEST_WORKSPACE_FOLDER = "test-workspace/aether";
	private static final String TEST_DATA_FOLDER = "test-data/aether";
	
	private static final String MVN_LOCAL_REPOSITORY = TEST_WORKSPACE_FOLDER + "/local-repo";
	private static final String LOCAL_TEST_ARTIFACT_GROUP_ID = "org.eclipse.aether";
	private static final String LOCAL_TEST_ARTIFACT_ARTIFACT_ID = "aether-impl";
	private static final String LOCAL_TEST_ARTIFACT_MAX_VERSION = "1.0.0.v20140518";
	private static final String LOCAL_TEST_ARTIFACT_MIN_VERSION = "0.9.1.v20140329";
	private static final String LOCAL_TEST_ARTIFACT_FULL_DESCRIPTOR = LOCAL_TEST_ARTIFACT_GROUP_ID 
			+ ":" + LOCAL_TEST_ARTIFACT_ARTIFACT_ID 
			+ ":" + LOCAL_TEST_ARTIFACT_MAX_VERSION;
	
	private static MavenRepositoryService mavenService;

	@BeforeClass
	public static void setUp() throws Exception
	{
		// Borrado inicial del repositorio local
		File databaseFolderFile = new File(MVN_LOCAL_REPOSITORY); 
		if(databaseFolderFile.exists()){
			FileUtils.deleteDirectory(databaseFolderFile);
		}
		
		mavenService = new MavenRepositoryService();
		mavenService.setUp(MVN_LOCAL_REPOSITORY);
		
		// Descarga de un artifact para las pruebas con el repositorio local
		mavenService.downloadArtifact(LOCAL_TEST_ARTIFACT_FULL_DESCRIPTOR);
		// También se descarga con una versión inferior para probar las versiones
		mavenService.downloadArtifact(LOCAL_TEST_ARTIFACT_GROUP_ID 
				+ ":" + LOCAL_TEST_ARTIFACT_ARTIFACT_ID 
				+ ":" + LOCAL_TEST_ARTIFACT_MIN_VERSION);
	}
	
	@Test
	@Ignore
	public void resolveLocalArtifactPathTest() throws Exception
	{
		String artifactRelativePath = mavenService.resolveLocalArtifactPath(LOCAL_TEST_ARTIFACT_FULL_DESCRIPTOR);	

		File expectedFile = new File(MVN_LOCAL_REPOSITORY + "/org/eclipse/aether/aether-impl/1.0.0.v20140518/aether-impl-1.0.0.v20140518.jar");
		Assert.assertEquals(artifactRelativePath, expectedFile.getAbsolutePath());		
	}
	
	@Test
	@Ignore
	public void resolveLocalArtifactDependenciesPathsTest() throws Exception
	{
		List<String> depsFiles = mavenService.resolveLocalArtifactDependenciesPaths(LOCAL_TEST_ARTIFACT_FULL_DESCRIPTOR);
		Assert.assertEquals(depsFiles.size(), 4);	
	}
	
	
	@Test
	public void resolveHighestLocalVersionTest() throws Exception
	{
		String version = mavenService.resolveLocalHighestVersion(LOCAL_TEST_ARTIFACT_GROUP_ID, LOCAL_TEST_ARTIFACT_ARTIFACT_ID);
		Assert.assertEquals(LOCAL_TEST_ARTIFACT_MAX_VERSION, version);		
	}
	
	@Test
	@Ignore
	public void resolveHighestVersionTest() throws Exception
	{
		// Atención al indicar el rango: Después de la coma no puede haber espacios en blanco!!
		String version = mavenService.resolveHighestVersion("commons-lang", "commons-lang", "[0,2.6)");
		Assert.assertEquals("2.5", version);
	}	 
}
