package com.bitsmi.snippets.tools.jgit;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.List;

import org.eclipse.jgit.api.CreateBranchCommand.SetupUpstreamMode;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.InitCommand;
import org.eclipse.jgit.lib.CommitBuilder;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.FileMode;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectInserter;
import org.eclipse.jgit.lib.PersonIdent;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.RefUpdate;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.lib.TreeFormatter;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.junit.Assert;
import org.junit.Test;

public class JgitTestCase 
{
	private static final String TEST_WORKSPACE_FOLDER = "test-workspace/jgit";
	private static final String TEST_DATA_FOLDER = "test-data/jgit";
	
	private static final String GIT_REPO_PATH = TEST_WORKSPACE_FOLDER + "/local_repo";
	
	@Test
	public void repositoryTest() throws Exception
	{
		Repository repository = null;
		ObjectInserter repoInserter = null;
		try
		{
			boolean initialize = !new File(GIT_REPO_PATH + "/.git").exists();
			if(initialize){
				InitCommand initCommand = Git.init();
				initCommand.setDirectory(new File(GIT_REPO_PATH));
				initCommand.setBare(false);
				initCommand.call();
			}
			repository = FileRepositoryBuilder.create(new File(GIT_REPO_PATH + "/.git"));
			
			if(initialize){
				List<Ref> call = new Git(repository).branchList().call();
				if(call.isEmpty()){
					RevCommit initialCommit = Git.wrap(repository).commit().setMessage("Initial commit").call();
					Git.wrap(repository)
					 	.branchCreate()
					 	.setName("master")
					 	.setUpstreamMode(SetupUpstreamMode.SET_UPSTREAM)
					 	.setStartPoint(initialCommit)
		                .setForce(true)
		                .call();
				 }
//				 for(Ref ref:call){
//					 System.out.println("Branch-Before: " + ref + " " + ref.getName() + " " + ref.getObjectId().getName());
//				 }
			}

			repoInserter = repository.newObjectInserter();
			
		    // Add a blob to the repository
		    ObjectId blobId = repoInserter.insert(Constants.OBJ_BLOB,
					  "hello world".getBytes("UTF-8"));
		    ObjectId blob2Id = repoInserter.insert(Constants.OBJ_BLOB,
					  "world hello".getBytes("UTF-8"));

		    // Create a tree that contains the blob as file "hello.txt"
		    TreeFormatter treeFormatter = new TreeFormatter();
		    treeFormatter.append("org.fenrir.sync.test.hello", FileMode.REGULAR_FILE, blobId);
		    treeFormatter.append("com.domini.test.fhello", FileMode.REGULAR_FILE, blob2Id);
		    ObjectId treeId = repoInserter.insert(treeFormatter);

		    ObjectId head = repository.resolve(Constants.HEAD + "^{commit}");
		    // Create a commit that contains this tree
		    CommitBuilder commit = new CommitBuilder();
		    PersonIdent ident = new PersonIdent("Me", "me@example.com");
		    commit.setParentId(head);
		    commit.setTreeId(treeId);
		    commit.setCommitter(ident);
		    commit.setAuthor(ident);
		    commit.setMessage("This is a new commit!");

		    ObjectId commitId = repoInserter.insert(commit);
		    repoInserter.flush();
		    
		    RefUpdate ru = repository.updateRef(Constants.HEAD);
		    ru.setForceUpdate(true);
		    ru.setRefLogIdent(ident);
		    ru.setNewObjectId(commitId);
		    ru.setRefLogMessage("commit: Initial Commit", false);
		    ru.update(); 
		    
		    ByteArrayOutputStream bos = new ByteArrayOutputStream(); 
			repository.newObjectReader().open(blobId).copyTo(bos);
			String result = bos.toString("UTF-8");
			Assert.assertEquals("hello world", result);
		}
		finally{
			if(repoInserter!=null){
				repoInserter.release();
			}
		    if(repository!=null){
				repository.close();
			}
		}
	}	 
}
