package com.bitsmi.snippets;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.infra.Blackhole;

/**
 * <ul>
 * 	<li>
 * 		JMH by default makes several warm-up cycles before collecting the stats. 
 * 		Thus, it makes sure that the results are not completely random and jvm has performed optimizations.
 *  </li>
 *	<li>
 *		{@literal @}{@link benchmark} runs iteration over the code, and then collects the average. The more runs you make through the code, the better stats you will collect.
 *	</li>
 * 	<li>
 * 		Use {@link Blackhole} class of JMH can avoid deal code elimination by jvm. 
 * 		If you pass the calculated results to {@link Blackhole#consume()}, it would trick the jvm. 
 * 		jvm will never drop the code, thinking that consume() method uses the result
 * 	</li>
 * </ul>
 */
public class SimpleBenchmark 
{
	private static final List<Integer> list = IntStream.rangeClosed(1, Integer.MAX_VALUE/100)
			.boxed()
			.collect(Collectors.toList());
	
	@Benchmark
	@BenchmarkMode(Mode.AverageTime)
	public static double conventionalLoop(Blackhole blackhole) 
	{
		double sum = 0;
		for (int i = 0; i < list.size(); i++) {
			sum += list.get(i);
		}

		blackhole.consume(sum);
		return sum;
	}

	@Benchmark
	@BenchmarkMode(Mode.AverageTime)
	public static double enhancedForLoop(Blackhole blackhole) throws InterruptedException 
	{
		double sum = 0;
		for (int integer : list) {
			sum += integer;
		}
		
		blackhole.consume(sum);
		return sum;
	}

	@Benchmark
	@BenchmarkMode(Mode.AverageTime)
	public static double streamMap(Blackhole blackhole) 
	{
		double sum = list.stream().mapToDouble(Integer::doubleValue).sum();
		
		blackhole.consume(sum);
		return sum;
	}
}
