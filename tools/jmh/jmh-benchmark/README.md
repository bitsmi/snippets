
## Generate project

```sh
mvn archetype:generate -DarchetypeGroupId=org.openjdk.jmh -DarchetypeArtifactId=jmh-java-benchmark-archetype -DinteractiveMode=false -DgroupId=com.bitsmi.snippets -DartifactId=snippets-jmh-benchmark -Dversion=1.0.0-FINAL
```

## Configure Fork, Warmup, and Iterations

The benchmark is executing 5 times, with 5 warmup iterations and 5 measurement iterations by default. 
JMH even allows to configure these values using @Fork, @Warmup and @Measurement annotations at type or method level. 

```java
@Fork(value = 2)
@Warmup(iterations = 2)
@Measurement(iterations = 3)
```

## Generate shaded jar

```sh 
mvn clean package
```

This will generate `benchmark.jar` that can be executed with:

```sh
java -jar benchmark.jar
```

## References

[Dzone](https://dzone.com/articles/java-microbenchmark-harness-jmh)
[JMH Samples](https://github.com/openjdk/jmh/tree/master/jmh-samples/src/main/java/org/openjdk/jmh/samples)