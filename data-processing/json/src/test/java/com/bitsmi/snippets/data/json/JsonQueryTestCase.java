package com.bitsmi.snippets.data.json;

import java.io.File;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.ReadContext;

public class JsonQueryTestCase 
{
	private static final String TEST_WORKSPACE_FOLDER = "test-workspace/json";
	private static final String TEST_DATA_FOLDER = "test-data/json";

	@Test
	public void simpleValueTest() throws Exception
	{	
		String json = null;
		try(InputStream is = FileUtils.openInputStream(new File(TEST_DATA_FOLDER + "/sample-data.json"))){
			json = IOUtils.toString(is, StandardCharsets.UTF_8);
		}
		
		Configuration conf = Configuration.defaultConfiguration()
				.addOptions(Option.ALWAYS_RETURN_LIST);
		ReadContext ctx = JsonPath.parse(json, conf);
		
		// Wildcard always returns an array
		List<String> simpleValue = ctx.read("$.Profile.SimpleLevel");
		List<String> nationality = ctx.read("$..BIOData.Nationality");
		List<Integer> postalCode = ctx.read("$..AddressInfo.Address[0].PostalCode");
		
		Assert.assertEquals("Simple value", simpleValue.get(0));
		Assert.assertEquals("ES", nationality.get(0));
		Assert.assertEquals((Integer)101011, postalCode.get(0));
	}	
}
