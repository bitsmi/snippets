package com.bitsmi.snippets.data.xml.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.URIResolver;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.FileUtils;

public class FileURIResolver implements URIResolver, AutoCloseable 
{
	private Map<String, String> documents = new HashMap<String, String>();

	private List<FileInputStream> inputStreams = new ArrayList<>(); 
	
    public FileURIResolver put(final String href, final String document) 
    {
        documents.put(href, document);
        return this;
    }

    @Override
    public Source resolve(final String href, final String base) throws TransformerException 
    {
        final String str = documents.get(href);
        if(str!=null){
        	try {
        		FileInputStream is = FileUtils.openInputStream(new File(str));
        		inputStreams.add(is);
        		return new StreamSource(is);
        	}
        	catch(IOException e) {
        		// null will be returned
        	}
        }
        return null;
    }

	@Override
	public void close() throws Exception 
	{
		for(FileInputStream is:inputStreams) {
			is.close();
		}
	}
}