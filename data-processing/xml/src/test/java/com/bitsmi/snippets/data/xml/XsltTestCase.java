package com.bitsmi.snippets.data.xml;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Paths;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.io.FileUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.bitsmi.snippets.data.xml.util.FileURIResolver;

public class XsltTestCase 
{
	private static final String TEST_WORKSPACE_FOLDER = "test-workspace/xml";
	private static final String TEST_DATA_FOLDER = "test-data/xml";

	@BeforeClass
	public static void setUp() throws IOException
	{
		// Clean test workspace
		FileUtils.deleteDirectory(Paths.get(TEST_WORKSPACE_FOLDER).toFile());
	}
	
	@Test
	public void simpleTransformationTest() throws Exception
	{
		try(InputStream sourceIs = FileUtils.openInputStream(new File(TEST_DATA_FOLDER + "/xslt/simple-transformation-test.xml"));
				InputStream xslIs = FileUtils.openInputStream(new File(TEST_DATA_FOLDER + "/xslt/simple-transformation-test.xsl"));
				OutputStream resultOs = FileUtils.openOutputStream(new File(TEST_WORKSPACE_FOLDER + "/xslt/simple-transformation-test.xml"))){						
			Source source = new StreamSource(sourceIs);
			Source xsl = new StreamSource(xslIs);
			Result result = new StreamResult(resultOs);
			
			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer(xsl);
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.transform(source, result);
		}
		
		Document document = loadDocument(TEST_WORKSPACE_FOLDER + "/xslt/simple-transformation-test.xml");
		assertXpath(document, "//body/p[4]/div[2]/span", "Gary Moore");
	}
	
	@Test
	public void mergeTransformationTest() throws Exception
	{
		try(InputStream source1Is = FileUtils.openInputStream(new File(TEST_DATA_FOLDER + "/xslt/merge-transformation-test-source1.xml"));
				InputStream xslIs = FileUtils.openInputStream(new File(TEST_DATA_FOLDER + "/xslt/merge-transformation-test.xsl"));
				FileURIResolver documentResolver = new FileURIResolver();
				OutputStream resultOs = FileUtils.openOutputStream(new File(TEST_WORKSPACE_FOLDER + "/xslt/merge-transformation-test.xml"));){						
			Source source1 = new StreamSource(source1Is);
			Source xsl = new StreamSource(xslIs);
			Result result = new StreamResult(resultOs);
			
			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer(xsl);
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setURIResolver(documentResolver.put("document2", TEST_DATA_FOLDER + "/xslt/merge-transformation-test-source2.xml"));
			transformer.transform(source1, result);
		}
		
		Document document = loadDocument(TEST_WORKSPACE_FOLDER + "/xslt/merge-transformation-test.xml");
		assertXpath(document, "//msg[2]/key", "BBB");
	}
	
	private Document loadDocument(String file) throws IOException, ParserConfigurationException, SAXException
	{
		try(FileInputStream fis = FileUtils.openInputStream(new File(file))){
			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = builderFactory.newDocumentBuilder();
			
			Document document = builder.parse(fis);
			
			return document;
		}
	}
	
	private void assertXpath(Document document, String expression, String expectedResult) throws XPathExpressionException
	{
		XPath xPath = XPathFactory.newInstance().newXPath();
		String result = (String)xPath.compile(expression).evaluate(document, XPathConstants.STRING);
		
		assertEquals(expectedResult, result);
	}
}
