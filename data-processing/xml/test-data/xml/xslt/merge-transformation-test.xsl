<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output indent="yes" />
	<xsl:variable name="document2" select="document('document2')" />

	<xsl:template match="@*|node()">
		<!-- copy the current node to the result tree -->
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="msg">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
			<xsl:text>  </xsl:text>
			<text>
				<xsl:value-of select="$document2/messages/msg[key=current()/key]/text" />
			</text>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>