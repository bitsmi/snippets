package com.bitsmi.snippets.patterns.chainofresponsability.spring.service;

public class TranslationChainMessage 
{
	private String language;
	private String message;
	
	private String translation;
	private String processorName;
	
	public String getLanguage() 
	{
		return language;
	}
	
	public void setLanguage(String language) 
	{
		this.language = language;
	}
	
	public String getMessage() 
	{
		return message;
	}
	
	public void setMessage(String message) 
	{
		this.message = message;
	}
	
	public String getTranslation()
	{
		return translation;
	}
	
	public void setTranslation(String translation)
	{
		this.translation = translation;
	}
	
	public String getProcessorName()
	{
		return processorName;
	}
	
	public void setProcessorName(String processorName)
	{
		this.processorName = processorName;
	}
}
