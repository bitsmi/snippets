package com.bitsmi.snippets.patterns.chainofresponsability.spring.service.delegate;

import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import com.bitsmi.snippets.patterns.chainofresponsability.spring.service.IChainExecutionElement;
import com.bitsmi.snippets.patterns.chainofresponsability.spring.service.TranslationChainMessage;
import com.google.common.collect.ImmutableMap;

@Component
public class GermanMessageProcessor implements IChainExecutionElement 
{
	private Map<String,String> translationMap;
	
	@PostConstruct
	private void setUp()
	{
		translationMap = ImmutableMap.<String, String>builder()
			    .put("Hallo", "Hola") 
			    .put("Auf Wiedersehen", "Adios") 
			    .put("Auto", "Coche") 
			    .put("Haus", "Casa")
			    .build();
	}
	
	@Override
	public boolean doChain(TranslationChainMessage action) throws Exception 
	{
		// Sólo se procesará en caso que el idioma sea el esperado y se tenga la traducción para el mensaje recibido
		if(!"de".equals(action.getLanguage())
				|| !translationMap.containsKey(action.getMessage())){
			return false;
		}
		
		action.setTranslation(translationMap.get(action.getMessage()));
		action.setProcessorName("GermanMessageProcessor");
		
		return true;
	}
}
