package com.bitsmi.snippets.patterns.chainofresponsability.spring.service;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;
import org.springframework.stereotype.Service;

@Service
public class ChainExecutionService 
{
	@Autowired
	private List<IChainExecutionElement> chain;
	
	@PostConstruct
    public void init() 
	{
        Collections.sort(chain, AnnotationAwareOrderComparator.INSTANCE);
    }
	
	public void setChain(List<IChainExecutionElement> chain)
	{
		this.chain = chain;
	}
	
	public void executeChain(TranslationChainMessage action) throws Exception
	{
		boolean breakLoop = false;
		Iterator<IChainExecutionElement> iterator = chain.iterator();
		while(iterator.hasNext() && !breakLoop){
			IChainExecutionElement delegate = iterator.next();
			if(delegate.doChain(action)){
				breakLoop = true;
			}
		}
		
		if(!breakLoop){
			// No se ha encontrado ninguna implementación para tratar el elemento
			throw new Exception("No se ha encontrado ninguna implementación para tratar el elemento");
		}
	}
}
