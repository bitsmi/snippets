package com.bitsmi.snippets.patterns.transformer;

import java.util.function.Function;

@FunctionalInterface
public interface ITransformer<T> 
{
	public <R> R by(Function<? super T, ? extends R> func);
}
