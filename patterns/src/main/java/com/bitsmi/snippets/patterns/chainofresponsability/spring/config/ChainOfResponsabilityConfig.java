package com.bitsmi.snippets.patterns.chainofresponsability.spring.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"com.bitsmi.snippets.patterns.chainofresponsability.spring"})
public class ChainOfResponsabilityConfig 
{
	
}
