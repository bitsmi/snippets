package com.bitsmi.snippets.patterns.chainofresponsability.spring.service;

public interface IChainExecutionElement 
{
	public boolean doChain(TranslationChainMessage action) throws Exception;
}
