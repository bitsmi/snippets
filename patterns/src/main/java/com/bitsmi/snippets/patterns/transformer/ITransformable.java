package com.bitsmi.snippets.patterns.transformer;

public interface ITransformable 
{
	public ITransformer<?> transformed();
}
