package com.bitsmi.snippets.patterns.chainofresponsability.spring.service.delegate;

import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.bitsmi.snippets.patterns.chainofresponsability.spring.service.IChainExecutionElement;
import com.bitsmi.snippets.patterns.chainofresponsability.spring.service.TranslationChainMessage;
import com.google.common.collect.ImmutableMap;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class EnglishGreetingsMessageProcessor implements IChainExecutionElement 
{
	private Map<String,String> translationMap;
	
	@PostConstruct
	private void setUp()
	{
		translationMap = ImmutableMap.<String, String>builder()
			    .put("Hello", "Hola") 
			    .put("Goodbye", "Adios") 
			    .build();
	}
	
	@Override
	public boolean doChain(TranslationChainMessage action) throws Exception 
	{
		// Sólo se procesará en caso que el idioma sea el esperado y se tenga la traducción para el mensaje recibido
		if(!"en".equals(action.getLanguage())
				|| !translationMap.containsKey(action.getMessage())){
			return false;
		}
		
		action.setTranslation(translationMap.get(action.getMessage()));
		action.setProcessorName("EnglishGreetingsMessageProcessor");
		
		return true;
	}
}
