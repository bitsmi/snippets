package com.bitsmi.snippets.patterns;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.function.Function;

import org.junit.jupiter.api.Test;

import com.bitsmi.snippets.patterns.transformer.ITransformable;
import com.bitsmi.snippets.patterns.transformer.ITransformer;

public class TransformerTestCase 
{
	@Test
	public void transformTest()
	{
		TransformableSourcePojo srcPojo = new TransformableSourcePojo()
				.setSourceValue1("Value 1")
				.setSourceValue2("Value 2");
		
		TargetPojo targetPojo = srcPojo.transformed()
				.by(SourcePojoTransformerFactory.toTargetPojo());
		
		assertThat(targetPojo.getTargetValue1()).isEqualTo(srcPojo.getSourceValue1());
		assertThat(targetPojo.getTargetValue2()).isEqualTo(srcPojo.getSourceValue2());
	}
	
	public static class TransformableSourcePojo implements ITransformable
	{
		private String sourceValue1;
		private String sourceValue2;
		
		public String getSourceValue1() 
		{
			return sourceValue1;
		}

		public TransformableSourcePojo setSourceValue1(String sourceValue1) 
		{
			this.sourceValue1 = sourceValue1;
			return this;
		}

		public String getSourceValue2() 
		{
			return sourceValue2;
		}

		public TransformableSourcePojo setSourceValue2(String sourceValue2) 
		{
			this.sourceValue2 = sourceValue2;
			return this;
		}

		@Override
		public ITransformer<TransformableSourcePojo> transformed() 
		{
			return this::transform;
		}
		
		private <R> R transform(Function<? super TransformableSourcePojo, ? extends R> f) 
		{
			return f.apply(this);
		}
	}
	
	public static class TargetPojo
	{
		private String targetValue1;
		private String targetValue2;
		
		public String getTargetValue1() 
		{
			return targetValue1;
		}
		
		public TargetPojo setTargetValue1(String targetValue1) 
		{
			this.targetValue1 = targetValue1;
			return this;
		}
		
		public String getTargetValue2() 
		{
			return targetValue2;
		}
		
		public TargetPojo setTargetValue2(String targetValue2) 
		{
			this.targetValue2 = targetValue2;
			return this;
		}
	}
	
	public static class SourcePojoTransformerFactory
	{
		public static Function<? super TransformableSourcePojo, ? extends TargetPojo> toTargetPojo()
		{
			return src -> {
				return new TargetPojo()
						.setTargetValue1(src.getSourceValue1())
						.setTargetValue2(src.getSourceValue2());
			};
		}		
	}
}
