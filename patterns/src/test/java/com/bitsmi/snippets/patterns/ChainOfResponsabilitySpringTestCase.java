package com.bitsmi.snippets.patterns;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bitsmi.snippets.patterns.chainofresponsability.spring.config.ChainOfResponsabilityConfig;
import com.bitsmi.snippets.patterns.chainofresponsability.spring.service.ChainExecutionService;
import com.bitsmi.snippets.patterns.chainofresponsability.spring.service.TranslationChainMessage;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={ChainOfResponsabilityConfig.class})
public class ChainOfResponsabilitySpringTestCase 
{
	@Autowired
	private ChainExecutionService executionService;
	
	@Test
	public void translationTest() throws Exception
	{
		TranslationChainMessage message1 = new TranslationChainMessage();
		message1.setLanguage("en");
		message1.setMessage("Car");
		executionService.executeChain(message1);
		Assert.assertEquals("Coche", message1.getTranslation());
		Assert.assertEquals("EnglishMessageProcessor", message1.getProcessorName());
		
		TranslationChainMessage message2 = new TranslationChainMessage();
		message2.setLanguage("de");
		message2.setMessage("Auto");
		executionService.executeChain(message2);
		Assert.assertEquals("Coche", message2.getTranslation());
		Assert.assertEquals("GermanMessageProcessor", message2.getProcessorName());
		
		TranslationChainMessage message3 = new TranslationChainMessage();
		message3.setLanguage("en");
		message3.setMessage("Hello");
		executionService.executeChain(message3);
		Assert.assertEquals("Hola", message3.getTranslation());
		Assert.assertEquals("EnglishGreetingsMessageProcessor", message3.getProcessorName());
	} 
}
