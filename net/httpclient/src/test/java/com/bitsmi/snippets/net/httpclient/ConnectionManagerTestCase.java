package com.bitsmi.snippets.net.httpclient;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Stopwatch;

public class ConnectionManagerTestCase 
{
	private static final String TEST_WORKSPACE_FOLDER = "test-workspace/httpclient";
	private static final String TEST_DATA_FOLDER = "test-data/httpclient";
	
	private final Logger log = LoggerFactory.getLogger(ConnectionManagerTestCase.class);

	@BeforeClass
	public static void setUp() throws IOException
	{
		// Clean test workspace
		FileUtils.deleteDirectory(Paths.get(TEST_WORKSPACE_FOLDER).toFile());
	}
	
	@Test
	public void poolingHttpClientConnectionManagerTest() throws IOException
	{
		/* Connection manager */
		PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
		// Increase max total connection to 200
		connectionManager.setMaxTotal(200);
		// Increase default max connection per route to 20
		connectionManager.setDefaultMaxPerRoute(20);
		// Increase max connections for route to 50
		HttpHost route = new HttpHost("www.google.com", 443);
		connectionManager.setMaxPerRoute(new HttpRoute(route), 50);

		/* Request config */
		RequestConfig requestConfig = RequestConfig.custom()
				// Determines the timeout in milliseconds until a connection is established
				.setConnectTimeout(1000)
				// Defines the socket timeout (SO_TIMEOUT) in milliseconds, which is the timeout for waiting for data or, put differently, a maximum period inactivity between two consecutive data packets)
				.setSocketTimeout(5000)
                .build();
		
		try(CloseableHttpClient client = HttpClientBuilder.create()
				.setConnectionManager(connectionManager)
        		.setDefaultRequestConfig(requestConfig)
        		.build()){
			Stopwatch stopwatch = Stopwatch.createStarted();
	
			/* Request managed route */
			long millis = doHeadRequest(client, stopwatch, "https://google.com");
	        log.info("MANAGED CONNECTION executed in {}ms", millis);
	        
	        /* Request managed route (2) */
	        stopwatch.reset().start();
	        millis = doHeadRequest(client, stopwatch, "https://google.com");
	        log.info("MANAGED CONNECTION(2) executed in {}ms", millis);
	        
	        /* Request unmanaged route */
	        stopwatch.reset().start();
	        millis = doHeadRequest(client, stopwatch, "https://bing.com");
	        log.info("MANAGED CONNECTION executed in {}ms", millis);
	        
	        /* Request unmanaged route (2) */
	        stopwatch.reset().start();
	        millis = doHeadRequest(client, stopwatch, "https://bing.com");
	        log.info("MANAGED UNCONNECTION(2) executed in {}ms", millis);
	        
	        stopwatch.stop();
		}
	}
	
	private long doHeadRequest(HttpClient client, Stopwatch stopwatch, String url) throws IOException
	{
		HttpHead request = new HttpHead(url);
        HttpResponse response = client.execute(request);
        int responseCode = response.getStatusLine().getStatusCode();
        log.info("Response code: {}", responseCode);
        return stopwatch.elapsed(TimeUnit.MILLISECONDS);
	}
}
