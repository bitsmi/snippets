package snippets.mrjar;

public class JavaVersionProviderImpl implements IJavaVersionProvider
{
    @Override
    public String getProvider()
    {
        return "Default provider";
    }
    
    @Override
    public String getVersion()
    {
        return System.getProperty("java.version");
    }
}
