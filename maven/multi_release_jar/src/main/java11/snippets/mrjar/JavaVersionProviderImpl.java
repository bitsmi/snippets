package snippets.mrjar;

public class JavaVersionProviderImpl implements IJavaVersionProvider
{
    @Override
    public String getProvider()
    {
        return "Java11 provider";
    }
    
    @Override
    public String getVersion()
    {
        // This API is not compatible with Java 8
        return Runtime.version().toString();
    }
}
