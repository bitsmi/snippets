package snippets.mrjar;

public interface IJavaVersionProvider
{
    public String getProvider();
    public String getVersion();
}
