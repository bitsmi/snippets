package snippets.mrjar;

public class MainProgram
{
    public static void main(String... args)
    {
        IJavaVersionProvider provider = new JavaVersionProviderImpl();
        
        StringBuilder versionBuilder = new StringBuilder("Java version: ")
                .append(provider.getProvider())
                .append(" / ")
                .append(provider.getVersion());
        
        System.out.println(versionBuilder.toString());
    }
}
