package snippets.mrjar;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

import org.junit.jupiter.api.Test;

public class JavaVersionProviderITCase
{
    @Test
    public void java11Test()
    {
        int version = getJREMajorVersion();
        assumeTrue(version>=11);
        
        IJavaVersionProvider provider = new JavaVersionProviderImpl();
        
        assertEquals(provider.getProvider(), "Java11 provider");
    }
    
    @Test
    public void defaultTest()
    {
        int version = getJREMajorVersion();
        assumeTrue(version<11);
        
        IJavaVersionProvider provider = new JavaVersionProviderImpl();
        
        assertEquals(provider.getProvider(), "Default provider");
    }
    
    private int getJREMajorVersion()
    {
        String javaVersion = System.getProperty( "java.version" );
        String major = javaVersion.substring(0, javaVersion.indexOf("."));
        return Integer.parseInt(major);
    }
}
