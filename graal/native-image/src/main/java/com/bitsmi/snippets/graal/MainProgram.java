package com.bitsmi.snippets.graal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MainProgram 
{
	private static final Logger log = LoggerFactory.getLogger(MainProgram.class);
	
	public static void main(String... args)
	{
		log.info("Hello from Graal");
	}
}
