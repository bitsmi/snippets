```sh
wget https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-21.0.0/graalvm-ce-java11-linux-amd64-21.0.0.tar.gz
tar -xzf graalvm-ce-java11-linux-amd64-21.0.0.tar.gz
```


Edit .bashrc

```
export PATH=/home/scrapbook/graalvm-ce-java11-21.0.0/bin:$PATH
```

Verify with `which java`

INSTALL NATIVE IMAGE

```sh
gu install native-image
```

Install dependencies

```sh
sudo apt-get install build-essential libz-dev zlib1g-dev
```

BUILD

```sh
native-image -H:ReflectionConfigurationFiles -H:ResourceConfigurationFiles --allow-incomplete-classpath --no-fallback -H:+ReportExceptionStackTraces -jar /vagrant/snippets-graal-1.0.0-SNAPSHOT.jar snippets
```

Analysis

java -agentlib:native-image-agent=config-merge-dir=analysis-output -jar /vagrant/snippets-graal-1.0.0-SNAPSHOT.jar