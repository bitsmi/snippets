package com.bitsmi.snippets.testing.junit.spring.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.bitsmi.snippets.testing.junit.spring")
public class SpringServiceConfig 
{

}
