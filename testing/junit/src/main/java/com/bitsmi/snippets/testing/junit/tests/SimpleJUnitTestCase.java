package com.bitsmi.snippets.testing.junit.tests;

import static org.assertj.core.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimpleJUnitTestCase 
{
	private final Logger log = LoggerFactory.getLogger(SimpleJUnitTestCase.class);
	
	@Test
	public void failTest()
	{
		log.debug("[failTest] Executing fail test");
		
		fail("Sample fail");
	}
}
