package com.bitsmi.snippets.testing.junit;

import java.util.HashMap;
import java.util.Map;

import org.junit.platform.engine.TestExecutionResult;
import org.junit.platform.launcher.TestExecutionListener;
import org.junit.platform.launcher.TestIdentifier;
import org.junit.platform.launcher.TestPlan;

public class JUnit5TestExecutionListener implements TestExecutionListener 
{
	private TestPlan testPlan;
	private Map<TestIdentifier, TestResult> results = new HashMap<>();
	private long startMillis;
	private long endMillis;
	
	@Override
	public void testPlanExecutionStarted(TestPlan testPlan) 
	{
		this.testPlan = testPlan;
		this.startMillis = System.currentTimeMillis();
	}
	
	@Override
	public void testPlanExecutionFinished(TestPlan testPlan) 
	{
		this.endMillis = System.currentTimeMillis();
	}

	@Override
	public void executionSkipped(TestIdentifier testIdentifier, String reason) 
	{
		if (testIdentifier.isTest()) {
			results.put(testIdentifier, new TestResult(testIdentifier, reason));
		}
	}

	@Override
	public void executionStarted(TestIdentifier testIdentifier) 
	{
		if (testIdentifier.isTest()) {
			results.put(testIdentifier, new TestResult(testIdentifier, System.currentTimeMillis()));
		}
	}

	@Override
	public void executionFinished(TestIdentifier testIdentifier, TestExecutionResult testExecutionResult) 
	{
		TestResult result = results.get(testIdentifier);
		if(result!=null) {
			result.setEndMillis(System.currentTimeMillis());
			result.setExecutionResult(testExecutionResult);
		}
	}
	
	public TestPlan getTestPlan()
	{
		return testPlan;
	}
	
	public long getElapsedTime()
	{
		if(endMillis==0) {
			return 0;
		}
		
		return endMillis - startMillis;
	}
	
	public TestResult getTestResult(TestIdentifier testIndentifier)
	{
		return results.get(testIndentifier);
	}
	
	public static final class TestResult
	{
		private TestIdentifier id;
		private TestExecutionResult executionResult;
		private boolean skipped = false;
		private String skippedReason;
		private long startMillis;
		private long endMillis;
		
		public TestResult(TestIdentifier id, String skippedReason)
		{
			this.id = id;
			this.skipped = true;
			this.skippedReason = skippedReason;
		}
		
		public TestResult(TestIdentifier id, long startMillis)
		{
			this.id = id;
			this.skipped = false;
			this.startMillis = startMillis;
		}
		
		public TestIdentifier getId()
		{
			return id;
		}
		
		public TestExecutionResult getExecutionResult()
		{
			return executionResult;
		}
		
		public void setExecutionResult(TestExecutionResult executionResult)
		{
			this.executionResult = executionResult;
		}
		
		public boolean isSkipped()
		{
			return skipped;
		}
		
		public String getSkippedReason()
		{
			return skippedReason;
		}
		
		public void setEndMillis(long endMillis)
		{
			this.endMillis = endMillis;
		}
		
		public long getElapsedTime()
		{
			if(endMillis==0) {
				return 0;
			}
			
			return endMillis - startMillis;
		}
	}
}
