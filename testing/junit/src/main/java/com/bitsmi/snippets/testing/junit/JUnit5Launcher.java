package com.bitsmi.snippets.testing.junit;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.platform.engine.discovery.ClassNameFilter;
import org.junit.platform.engine.discovery.DiscoverySelectors;
import org.junit.platform.launcher.Launcher;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.junit.platform.launcher.TestIdentifier;
import org.junit.platform.launcher.TestPlan;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;
import org.junit.platform.launcher.core.LauncherFactory;

import com.bitsmi.snippets.testing.junit.JUnit5TestExecutionListener.TestResult;

public class JUnit5Launcher 
{
	public static void main(String... args)
	{
		Launcher launcher = LauncherFactory.create();
		
		LauncherDiscoveryRequest request = LauncherDiscoveryRequestBuilder.request()
			      .selectors(DiscoverySelectors.selectPackage("com.bitsmi.snippets.testing.junit.tests"))
			      .filters(ClassNameFilter.includeClassNamePatterns(".*TestCase"))
			      .build();
//		TestPlan testPlan = launcher.discover(request);
		JUnit5TestExecutionListener listener = new JUnit5TestExecutionListener();
		launcher.registerTestExecutionListeners(listener);
		launcher.execute(request);
		
		TestPlan testPlan = listener.getTestPlan();
		Set<TestPlanNode> testNodes = Stream.concat(testPlan.getRoots().stream(), 
					testPlan.getRoots()
						.stream()	
						.flatMap(id -> testPlan.getDescendants(id).stream())
				)
				.map(TestPlanNode::new)
				.collect(Collectors.toSet());
			
		Map<String, List<TestPlanNode>> childrenAggregation = testNodes.stream()
				.filter(n -> n.getId().getParentId().isPresent())
				.collect(Collectors.groupingBy(n -> n.getId().getParentId().get()));
		
		Iterator<TestPlanNode> iterator = testNodes.iterator();
		while(iterator.hasNext()) {
			TestPlanNode node = iterator.next();
			node.setResult(listener.getTestResult(node.getId()));
			node.setChildren(childrenAggregation.getOrDefault(node.getId().getUniqueId(), Collections.emptyList()));
			// Leave only root nodes
			if(node.getId().getParentId().isPresent()) {
				iterator.remove();
			}
		}		
		
		// Print results
		for(TestPlanNode node:testNodes) {
			walkTestPlanNodes(node, 0);
		}
	}
	
	public static void walkTestPlanNodes(TestPlanNode node, int level)
	{		
		String tabs = "" + Stream.generate(() -> "\t").limit(level).collect(Collectors.joining());		
		System.out.println(tabs 
				+ (node.getId().isContainer() ? "[C]" : (node.getId().isTest() ? "[T]" : "[ ]"))
				+ node.getId().getDisplayName() 
				+ (node.getResult()!=null ? "(" + node.getResult().getElapsedTime() + "ms.): " :  "") 
				+ (node.getResult()!=null ? ": " + node.getResult().getExecutionResult().getStatus() : "")
		);
		
		for(TestPlanNode child:node.getChildren()) {
			walkTestPlanNodes(child, level+1);
		}
	}
	
	public static class TestPlanNode
	{
		private TestIdentifier id;
		private List<TestPlanNode> children;
		private TestResult result;
		
		public TestPlanNode(TestIdentifier id)
		{
			this.id = id;
		}
		
		public TestIdentifier getId()
		{
			return id;
		}
		
		public List<TestPlanNode> getChildren()
		{
			return children;
		}
		
		public void setChildren(List<TestPlanNode> children)
		{
			this.children = children;
		}
		
		public TestResult getResult()
		{
			return result;
		}
		
		public void setResult(TestResult result)
		{
			this.result = result;
		}
	}
}
