package com.bitsmi.snippets.testing.junit.spring.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bitsmi.snippets.testing.junit.spring.service.ISampleService;

@Service
public class SampleServiceImpl implements ISampleService 
{
	public static final String GREETINGS = "Hello from SampleServiceImpl";
	
	@Autowired
	public String getGreetings()
	{
		return GREETINGS;
	}
}
