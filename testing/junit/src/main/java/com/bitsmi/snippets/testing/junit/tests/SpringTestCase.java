package com.bitsmi.snippets.testing.junit.tests;

import static org.assertj.core.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.bitsmi.snippets.testing.junit.spring.config.SpringServiceConfig;
import com.bitsmi.snippets.testing.junit.spring.service.ISampleService;
import com.bitsmi.snippets.testing.junit.spring.service.impl.SampleServiceImpl;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(loader=AnnotationConfigContextLoader.class)
public class SpringTestCase 
{
	private final Logger log = LoggerFactory.getLogger(SpringTestCase.class);
	
	@Autowired
	private ISampleService sampleService;
	
	@Test
	public void springServiceTest()
	{
		log.debug("[springServiceTest] Executing String Service Test");
		
		String greetings = sampleService.getGreetings();
		
		assertThat(greetings).isEqualTo(SampleServiceImpl.GREETINGS);
	}
	
	/*--------------------------------*
     * SUPPORT METHODS AND CLASSES
     *--------------------------------*/
    @Configuration
    @Import({SpringServiceConfig.class})
    public static class TestConfig
    {
        
    }
}
