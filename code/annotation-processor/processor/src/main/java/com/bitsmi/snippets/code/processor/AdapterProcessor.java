package com.bitsmi.snippets.code.processor;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.auto.service.AutoService;

@SupportedAnnotationTypes("com.bitsmi.snippets.code.processor.Adapter")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
// populate META-INF/services/javax.annotation.processing.Processor
@AutoService(Processor.class)
public class AdapterProcessor extends AbstractProcessor 
{
	private final Logger log = LoggerFactory.getLogger(AdapterProcessor.class);
	
	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) 
	{
		// Only "Adapter" annotation as specified in @SupportedAnnotationTypes
		for (TypeElement annotation : annotations) {
			Set<? extends Element> elements = roundEnv.getElementsAnnotatedWith(annotation);
			// Adapter is a type annotation, so elements are all classes
			for(Element elem:elements) {
				log.info("processing element ({})", elem);
				createAdapter((DeclaredType)elem.asType());
			}
		}
		
		// Return "true" indicating that annotations were processed
		return true;
	}
	
	private void createAdapter(DeclaredType clazzInfo)
	{
		AnnotationMirror adapterAnnotation = getAnnotationMirror(asTypeElement(clazzInfo), Adapter.class);
	    AnnotationValue adapterAnnotationValue = getAnnotationValue(adapterAnnotation, "value");
	    TypeElement adaptedClass = asTypeElement((TypeMirror)adapterAnnotationValue.getValue());
		Name adapterName = ((TypeElement)clazzInfo.asElement()).getQualifiedName();
		String adapterClassName = adapterName.toString() + "Adapter";
		
		log.debug("Adapted class: {}", adaptedClass.getQualifiedName());
		
		AdapterModel model = new AdapterModel();
		model.packageName = adapterClassName.substring(0, adapterClassName.lastIndexOf("."));
		model.name = adapterClassName.substring(adapterClassName.lastIndexOf(".")+1, adapterClassName.length());
		model.adapterClass = asTypeElement(clazzInfo);
		model.adaptedClass = adaptedClass;
		
		String code = generateCode(model);
		
		try {
			JavaFileObject builderFile = processingEnv.getFiler()
					.createSourceFile(adapterClassName);
			try (PrintWriter out = new PrintWriter(builderFile.openWriter())) {
			    out.println(code);
			}
		}
		catch(IOException e) {
			processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR,
					String.format("Error writing adapter class: %s", e.getMessage()));
		}
	}
	
	private static AnnotationMirror getAnnotationMirror(TypeElement typeElement, Class<?> clazz) 
	{
	    String clazzName = clazz.getName();
	    for(AnnotationMirror m : typeElement.getAnnotationMirrors()) {
	        if(m.getAnnotationType().toString().equals(clazzName)) {
	            return m;
	        }
	    }
	    return null;
	}

	private static AnnotationValue getAnnotationValue(AnnotationMirror annotationMirror, String key) 
	{
	    for(Entry<? extends ExecutableElement, ? extends AnnotationValue> entry : annotationMirror.getElementValues().entrySet() ) {
	        if(entry.getKey().getSimpleName().toString().equals(key)) {
	            return entry.getValue();
	        }
	    }
	    return null;
	}
	
	private TypeElement asTypeElement(TypeMirror typeMirror) 
	{
	    Types TypeUtils = this.processingEnv.getTypeUtils();
	    return (TypeElement)TypeUtils.asElement(typeMirror);
	}
	
	private String generateCode(AdapterModel model)
	{
		StringBuilder builder = new StringBuilder();
		builder.append("package ").append(model.packageName).append(";").append("\n");
		builder.append("public class ").append(model.name);
		if(model.adapterClass.getKind()==ElementKind.INTERFACE) {
			builder.append(" implements ").append(model.adapterClass.getQualifiedName());
		}
		else {
			builder.append(" extends ").append(model.adapterClass.getQualifiedName());
		}
		builder.append("{\n");
			builder.append("\t").append("private ").append(model.adaptedClass.getSimpleName()).append(" value;\n");
			builder.append("\t").append("public ").append(model.name).append("(").append(model.adaptedClass.getQualifiedName()).append(" value) {\n");
				builder.append("\t\t").append("this.value = value;\n");
			builder.append("\t}\n");
			builder.append("\t").append("public String toString() {\n");
				builder.append("\t\t").append("return this.value.toString() + \" ADAPTED\";\n");
			builder.append("\t}\n");
		builder.append("}\n");
		
		return builder.toString();
	}
	
	private static class AdapterModel 
	{
		private String packageName;
		private String name;
		private TypeElement adapterClass;
		private TypeElement adaptedClass;
	}
}
