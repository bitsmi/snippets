package com.bitsmi.snippets.code.processor.example;

public class Entity 
{
	private String property;
	
	public Entity(String property)
	{
		this.property = property;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AdaptedEntity [property=").append(property).append("]");
		return builder.toString();
	}
}
