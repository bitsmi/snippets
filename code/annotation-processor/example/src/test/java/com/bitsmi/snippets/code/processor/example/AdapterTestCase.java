package com.bitsmi.snippets.code.processor.example;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AdapterTestCase 
{
	private static final String TEST_WORKSPACE_FOLDER = "test-workspace/asm";
	private static final String TEST_DATA_FOLDER = "test-data/asm";
	
	private final Logger log = LoggerFactory.getLogger(AdapterTestCase.class);

	@Test
	public void adapterTest() throws Exception
	{
		Entity entity = new Entity("TEST");
		EntityDtoAdapter adapter = new EntityDtoAdapter(entity);
		
		Assertions.assertEquals("AdaptedEntity [property=TEST] ADAPTED", adapter.toString());
	}
}
