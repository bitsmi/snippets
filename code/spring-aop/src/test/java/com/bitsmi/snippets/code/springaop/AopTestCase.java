package com.bitsmi.snippets.code.springaop;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bitsmi.snippets.code.springaop.config.AopConfig;
import com.bitsmi.snippets.code.springaop.service.IMessageService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={AopConfig.class})
public class AopTestCase
{
	private static final String TEST_WORKSPACE_FOLDER = "test-workspace/spring-aop";
	private static final String TEST_DATA_FOLDER = "test-data/spring-aop";
	
	@Autowired
	private IMessageService messageService;
	
	@Test
	public void translationTest() throws Exception
	{
		String message = messageService.getGreetings("FOO");
		
		Assert.assertEquals("INTERCEPTED: Hello FOO", message);
	}
}
