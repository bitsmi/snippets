package com.bitsmi.snippets.code.springaop.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class MethodAspect 
{
	private final Logger log = LoggerFactory.getLogger(MethodAspect.class);
	
	/**
	 * [method designator]([return type] snippets.code.spring.aop..[class].[method]([arguments]))
	 * execution: 
	 * return type '*': El Pointcut s'executarà per tots els mètodes independentment del tipus de retorn d'aquest 
	 * '..': indica que es tenen en compte els subpackages
	 * class '*':  El Pointcut s'executarà per totes les classes
	 * method '*': El Pointcut s'executarà per tots els mètodes
	 * arguments '..': El Pointcut s'executarà per tots els mètodes independentment del nombre i tipus dels arguments d'aquest
	 */
	@Pointcut("execution(* com.bitsmi.snippets.code.springaop..*.*(..))")
	public void methodLoggingPointcut(){ }
	
	/**
	 * Pointcut that matches all methods in classes of com.bitsmi.snippets.code.springaop.service package and subpackages that returns String
	 */
	@Pointcut("execution(String com.bitsmi.snippets.code.springaop.service..*.*(..))")
	public void stringReturnPointcut(){ }
	
	@Before("methodLoggingPointcut()")
    public void doMethodParamLogging(JoinPoint jp) 
	{
		Object[] args = jp.getArgs();
		StringBuilder builder = new StringBuilder("METHOD CALL:")
				.append(jp.getSignature().toLongString());
		if(args!=null){
			builder.append("ARGS: ");
			for(Object arg:args){
				builder.append("[").append(arg.toString()).append("]");
			}
		}
		
		log.info(builder.toString());
	}
	
	@Around("stringReturnPointcut()")
	public Object doStringReturn(final ProceedingJoinPoint joinPoint) throws Throwable
	{
		return "INTERCEPTED: " + joinPoint.proceed();
	}
}
