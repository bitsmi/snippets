package com.bitsmi.snippets.code.springaop.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@ComponentScan({"com.bitsmi.snippets.code.springaop"})
@EnableAspectJAutoProxy
public class AopConfig 
{
	
}
