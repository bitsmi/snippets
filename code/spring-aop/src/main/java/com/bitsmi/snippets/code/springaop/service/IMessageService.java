package com.bitsmi.snippets.code.springaop.service;

public interface IMessageService 
{
	public String getGreetings(String name); 
}
