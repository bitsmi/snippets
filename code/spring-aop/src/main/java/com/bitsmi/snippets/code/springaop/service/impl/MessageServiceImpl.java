package com.bitsmi.snippets.code.springaop.service.impl;

import org.springframework.stereotype.Service;

import com.bitsmi.snippets.code.springaop.service.IMessageService;

@Service
public class MessageServiceImpl implements IMessageService 
{
	@Override
	public String getGreetings(String name) 
	{
		return "Hello " + name;
	}
}
