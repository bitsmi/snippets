package com.bitsmi.snippets.code.cglib;

import org.junit.Assert;
import org.junit.Test;

import net.sf.cglib.proxy.Enhancer;

public class ProxyTestCase 
{
	private static final String TEST_WORKSPACE_FOLDER = "test-workspace/logback";
	private static final String TEST_DATA_FOLDER = "test-data/logback";
	
	@Test
	public void interceptorTest()
	{
		SamplePojo pojo = (SamplePojo)Enhancer.create(SamplePojo.class, new CustomInterceptor());
		
		Assert.assertEquals("INTERCEPTED", pojo.sayHello());
		Assert.assertEquals("Boo", pojo.sayBoo());
	}
}
