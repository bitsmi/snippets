package com.bitsmi.snippets.code.cglib;

import java.lang.reflect.Method;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

public class CustomInterceptor implements MethodInterceptor 
{
	@Override
	public Object intercept(Object object, Method method, Object[] args, MethodProxy methodProxy) throws Throwable 
	{		
		if("sayHello".equals(method.getName())){
			return "INTERCEPTED";
		}
		
		/* Invoca el m�tode original. 
		 * Si es fa servir invoke() la crida torna a passar per proxy. És útil si es vol fer la crida 
		 * sobre un objecte diferent
		 */
		return methodProxy.invokeSuper(object, args);
	}
}
