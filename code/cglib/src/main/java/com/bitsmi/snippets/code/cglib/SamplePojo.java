package com.bitsmi.snippets.code.cglib;

public class SamplePojo 
{
	public String sayHello()
	{
		return "Hello";
	}
	
	public String sayBoo()
	{
		return "Boo";
	}
}
