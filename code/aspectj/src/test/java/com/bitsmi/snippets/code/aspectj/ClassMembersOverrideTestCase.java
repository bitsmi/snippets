package com.bitsmi.snippets.code.aspectj;

import org.junit.Assert;
import org.junit.Test;

import com.bitsmi.snippets.code.aspectj.domain.CustomEntity;
import com.bitsmi.snippets.code.aspectj.domain.DefaultEntity;

public class ClassMembersOverrideTestCase 
{
	@Test
	public void defaultOverrideTest()
	{
		DefaultEntity entity = new DefaultEntity();
		Assert.assertEquals("From Aspect", entity.toString());
	}
	
	@Test
	public void customOverrideTest()
	{
		CustomEntity entity = new CustomEntity();
		Assert.assertEquals("From Entity", entity.toString());
	}
}
