package com.bitsmi.snippets.code.aspectj.domain.aspect;

import com.bitsmi.snippets.code.aspectj.domain.IEntityState;

public aspect EntityStateAspect
{
	private Long IEntityState.id;
	private Long IEntityState.version;
	
	public Long IEntityState.getId()
	{
		return this.id;
	}
	
	public void IEntityState.setId(Long id)
	{
		this.id = id;
	}
	
	public Long IEntityState.getVersion()
	{
		return this.version;
	}
	
	public void IEntityState.setVersion(Long version)
	{
		this.id = version;
	}
	
	public String IEntityState.toString()
	{
		return "From Aspect";
	}
}
