package com.bitsmi.snippets.code.aspectj.domain;

public interface IEntityState 
{
	public Long getId();
	public void setId(Long id);
	
	public Long getVersion();
	public void setVersion(Long version);
}
