package com.bitsmi.snippets.report.itext;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.io.font.PdfEncodings;
import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;

public class PdfMarkTestCase 
{
	private static final String TEST_WORKSPACE_FOLDER = "test-workspace/itext";
	private static final String TEST_DATA_FOLDER = "test-data/itext";
	
	private final Logger log = LoggerFactory.getLogger(PdfMarkTestCase.class);
	
	@BeforeClass
	public static void setUp() throws IOException
	{
		// Clean test workspace
		File workspaceFolder = Paths.get(TEST_WORKSPACE_FOLDER).toFile();
		FileUtils.deleteDirectory(workspaceFolder);
		FileUtils.forceMkdir(workspaceFolder);
	}
	
	@Test
	public void pdfMarkTest() throws Exception
	{
		Path template = Paths.get(TEST_DATA_FOLDER, "template.pdf");
		Path result = Paths.get(TEST_WORKSPACE_FOLDER, "pdfMarkTest.pdf");

		InputStream in = new FileInputStream(template.toFile());
		FileOutputStream out = new FileOutputStream(result.toFile());
	    
		PdfReader reader = new PdfReader(in);
		PdfWriter writer = new PdfWriter(out);
		
		try(PdfDocument srcDocument = new PdfDocument(reader, writer)){
			int numberOfPages = srcDocument.getNumberOfPages();
			for (int i = 1; i <= numberOfPages; i++) {
			    PdfPage page = srcDocument.getPage(i);
			    Rectangle pageSize = page.getPageSize();
			    PdfCanvas canvas = new PdfCanvas(page);
			    
  			    // Draw side text
			    // Max 90 chars
                float ly = (pageSize.getBottom() + pageSize.getHeight() - (90*4)) / 2;
                float lx = pageSize.getLeft() + 15;
			    canvas.beginText()
			    		.setFontAndSize(PdfFontFactory.createFont(StandardFonts.TIMES_ROMAN, PdfEncodings.WINANSI, false), 10)
			    		// Rotate and translate
			    		.setTextMatrix(0, 1f, -1f, 0, lx, ly)
			            .showText("Test side note")
			            .endText();
			    
			    // Draw footer text
			    ly = pageSize.getBottom() + 20;
                lx = pageSize.getLeft() + 100;
			    canvas.beginText().setFontAndSize(PdfFontFactory.createFont(StandardFonts.TIMES_BOLD, PdfEncodings.WINANSI, false), 9)
	    				.moveText(lx, ly)
	    				.showText("Test foot note")
	    				.endText();
			}
		}
		catch(Exception e) {
			log.error("ERROR: {}", e.getMessage(), e);
			throw e;
		}
		finally {
			writer.flush();
			reader.close();
			writer.close();
			
			in.close();
			out.close();
		}
		
		Assert.assertTrue(result.toFile().exists());
	}
}
